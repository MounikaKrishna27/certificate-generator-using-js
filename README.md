# Certificate Generator Using JS:
  This project is completely based on the Generation of an online certificate using front end concepts like Html,Css and Javascript only for people who lack knowledge in backend
Requirements: 
 1. Html
 2. Css
 3. JavaScript
 4. LiveServer
 First we need to install the LiveServer in the visual studio code to run the code..
 This project starts when we first click on the LiveServer it opens a webpage which asks the user for name and after submitting the name.
 The certificate downloading process starts with the name given by the user in the previous page.I used different styles to style the page.
 In index.js we have the code for generating the certificate..

 By using **const userName = document.getElementById("name");** the name entered by the user will be fetched and used in the generation of certificate.

 I added this **submitBtn.addEventListener("click", () => {
  const val = capitalize(userName.value);** to add an event to the submit button which when clicked takes the name given in the user input and places the value inside it in the certificate.
 Finally,
 **generatePDF** is used to generate the certificate in pdf format.

